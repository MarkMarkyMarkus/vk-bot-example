package com.markus.vkbot.service;

import com.markus.vkbot.controller.request.VkEventRequest;
import com.markus.vkbot.domain.Bot;
import com.markus.vkbot.domain.exception.VkApiCallException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.markus.vkbot.util.CallbackApiEvent.CONFIRMATION;
import static com.markus.vkbot.util.CallbackApiEvent.MESSAGE_NEW;

@Slf4j
@Service
@RequiredArgsConstructor
public class BotService {
    private final String OK_RESPONSE = "ok";
    private final Bot bot;

    public String handleRequest(final VkEventRequest request) {
        if (request == null || request.getType() == null) return OK_RESPONSE;

        return switch (request.getType()) {
            case CONFIRMATION -> bot.confirm();
            case MESSAGE_NEW -> {
                echoReplyToMessage(
                        request.getObject().getMessage().getPeerId(),
                        request.getObject().getMessage().getText()
                );
                yield OK_RESPONSE;
            }
            default -> {
                log.info("Unknown event type: {}", request.getType());
                yield OK_RESPONSE;
            }
        };
    }

    private void echoReplyToMessage(final int peerId, final String msg) {
        try {
            bot.sendMessage(peerId, "Вы написали: " + msg);
        } catch (VkApiCallException | IOException e) {
            log.error("Error sending message: {}", e.getMessage());
        }
    }
}
