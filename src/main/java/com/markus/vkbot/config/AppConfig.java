package com.markus.vkbot.config;

import com.markus.vkbot.domain.Bot;
import com.markus.vkbot.domain.VkBot;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(AppProperties.class)
public class AppConfig {
    private final AppProperties properties;

    @Bean
    public Bot bot() {
        return VkBot
                .builder()
                .baseApi(properties.getBaseUrl())
                .secret(properties.getSecret())
                .token(properties.getToken())
                .apiVersion(properties.getApiVersion())
                .build();
    }
}
