package com.markus.vkbot.domain;

import com.markus.vkbot.domain.exception.VkApiCallException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@DisplayName("VK bot unit tests")
class VkBotTest {
    @Autowired
    private Bot bot;

    @Test
    @DisplayName("Get correct secret key from properties")
    void confirmSecret() {
        final var secret = bot.confirm();
        assertEquals("test_secret", secret);
    }

    @Test
    @DisplayName("Send invalid message via VK API (invalid token)")
    void sendMessageWithInvalidCredentials() {
        assertThrows(
                VkApiCallException.class,
                () -> bot.sendMessage(0, "text")
        );
    }
}
