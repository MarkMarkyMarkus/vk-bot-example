package com.markus.vkbot.controller;

import com.markus.vkbot.controller.request.VkEventRequest;
import com.markus.vkbot.service.BotService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public final class BotController {
    private final BotService botService;

    @PostMapping("/api/v0/handler")
    public String handler(@RequestBody final VkEventRequest request) {
        return botService.handleRequest(request);
    }
}
