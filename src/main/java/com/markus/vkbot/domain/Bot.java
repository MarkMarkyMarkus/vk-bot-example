package com.markus.vkbot.domain;

import java.io.IOException;

public interface Bot {
    String confirm();

    void sendMessage(final int destinationId, final String msg) throws IOException;
}
