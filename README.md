# VK Bot

### About

Demo app for a bot using VK API / VK Callback API.

### Usage

Add credentials (secret word and access token) to
[application.properties](./src/main/resources/application.properties):

```
bot.secret=${VK_SECRET:YOUR SECRET HERE}
bot.token=${VK_TOKEN:YOUR TOKEN HERE}
```

Run:

```./gradlew bootRun```

### Demo

Since the bot deployed to Heroku, you can temporarily find it [here](https://vk.me/club197568610).
