package com.markus.vkbot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.markus.vkbot.controller.request.VkEventRequest;
import com.markus.vkbot.controller.request.VkEventRequestObj;
import com.markus.vkbot.service.BotService;
import com.markus.vkbot.util.CallbackApiEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = BotController.class)
@DisplayName("Bot controller unit tests")
class BotControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
    @MockBean
    private BotService service;

    @BeforeEach
    private void setUp() {
        Mockito.when(service.handleRequest(Mockito.any()))
                .thenReturn("ok");
    }

    @Test
    @DisplayName("Handle correct new message event")
    void correctEventWithNewMessage() throws Exception {
        final var newMessage = new VkEventRequest(
                CallbackApiEvent.MESSAGE_NEW,
                new VkEventRequestObj(
                        new VkEventRequestObj
                                .PrivateMessage(1, new Date().toInstant(), 0, 0, "Some text")
                ),
                0
        );
        mockMvc.perform(
                post("/api/v0/handler")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(newMessage))
        )
                .andExpect(status().isOk())
                .andExpect(content().string("ok"));
    }

    @Test
    @DisplayName("Handle correct confirmation event")
    void correctEventWithConfirmation() throws Exception {
        final var confirmation = new VkEventRequest(
                CallbackApiEvent.CONFIRMATION,
                null,
                0
        );
        mockMvc.perform(
                post("/api/v0/handler")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(confirmation))
        )
                .andExpect(status().isOk())
                .andExpect(content().string("ok"));
    }

    @Test
    @DisplayName("Handle unknown event")
    void unknownEvent() throws Exception {
        final var unknownEvent = new VkEventRequest(
                "unknown",
                null,
                0
        );
        mockMvc.perform(
                post("/api/v0/handler")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(unknownEvent))
        )
                .andExpect(status().isOk())
                .andExpect(content().string("ok"));
    }

    @Test
    @DisplayName("Handle invalid request")
    void invalidRequest() throws Exception {
        mockMvc.perform(
                post("/api/v0/handler")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.ALL)
                        .content(objectMapper.writeValueAsString("some text"))
        )
                .andExpect(status().is4xxClientError());
    }
}
