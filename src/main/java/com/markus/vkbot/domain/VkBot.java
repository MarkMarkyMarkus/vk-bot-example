package com.markus.vkbot.domain;

import com.markus.vkbot.domain.exception.VkApiCallException;
import com.markus.vkbot.domain.response.VkApiCallResponse;
import lombok.Builder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Random;

@Builder
public class VkBot implements Bot {
    private final String baseApi;
    private final String secret;
    private final String token;
    private final String apiVersion;

    @Override
    public String confirm() {
        return secret;
    }

    @Override
    public void sendMessage(final int peerId, final String msg)
            throws VkApiCallException, UnsupportedEncodingException {
        if (msg == null) return;
        final var params = new LinkedMultiValueMap<String, String>();
        params.add("peer_id", Integer.toString(peerId));
        params.add("random_id", Integer.toString(new Random().nextInt()));
        params.add("message", URLEncoder.encode(msg, StandardCharsets.UTF_8.toString()));

        vkApiCall("messages.send", HttpMethod.POST, params);
    }

    private void vkApiCall(final String vkMethod,
                           final HttpMethod httpMethod,
                           final MultiValueMap<String, String> params) throws VkApiCallException {
        final var restTemplate = new RestTemplate();
        final var headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        final var entity = new HttpEntity<>(headers);
        final var uri = UriComponentsBuilder
                .fromUriString(baseApi)
                .path("method/" + vkMethod)
                .queryParams(params)
                .queryParam("access_token", token)
                .queryParam("v", apiVersion)
                .build(true)
                .toUri();
        final var response = restTemplate
                .exchange(uri, httpMethod, entity, VkApiCallResponse.class);
        if (response.getStatusCode() != HttpStatus.OK)
            throw new VkApiCallException("Error calling VK API: " + response.getStatusCode());
        else if (Objects.requireNonNull(response.getBody()).getError() != null)
            throw new VkApiCallException(response.getBody().getError().toString());
    }
}
