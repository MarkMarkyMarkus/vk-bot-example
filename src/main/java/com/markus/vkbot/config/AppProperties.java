package com.markus.vkbot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "bot")
public class AppProperties {
    private String baseUrl;
    private String host;
    private String secret;
    private String token;
    private String apiVersion;
}
