package com.markus.vkbot.service;

import com.markus.vkbot.controller.request.VkEventRequest;
import com.markus.vkbot.controller.request.VkEventRequestObj;
import com.markus.vkbot.domain.Bot;
import com.markus.vkbot.util.CallbackApiEvent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@DisplayName("Bot service unit tests")
class BotServiceTest {
    @Autowired
    private BotService service;
    @MockBean
    private Bot bot;

    @Test
    @DisplayName("Handle correct new message event")
    void correctEventWithNewMessage() throws IOException {
        final var newMessage = new VkEventRequest(
                CallbackApiEvent.MESSAGE_NEW,
                new VkEventRequestObj(
                        new VkEventRequestObj
                                .PrivateMessage(1, new Date().toInstant(), 0, 0, "Some text")
                ),
                0
        );
        final var result = service.handleRequest(newMessage);
        assertEquals("ok", result);
        verify(bot, times(1))
                .sendMessage(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    @DisplayName("Handle correct confirmation event")
    void correctEventWithConfirmation() {
        final var confirmation = new VkEventRequest(
                CallbackApiEvent.CONFIRMATION,
                null,
                0
        );
        service.handleRequest(confirmation);
        verify(bot, times(1)).confirm();
    }

    @Test
    @DisplayName("Handle unknown event")
    void unknownEvent() throws IOException {
        final var unknownEvent = new VkEventRequest(
                "unknown",
                null,
                0
        );
        service.handleRequest(unknownEvent);
        verify(bot, times(0)).confirm();
        verify(bot, times(0)).sendMessage(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    @DisplayName("Handle invalid requests")
    void invalidRequest() throws IOException {
        final var unknownEvent = new VkEventRequest(
                null,
                null,
                0
        );
        service.handleRequest(unknownEvent);
        verify(bot, times(0)).confirm();
        verify(bot, times(0)).sendMessage(Mockito.anyInt(), Mockito.anyString());

        service.handleRequest(null);
        verify(bot, times(0)).confirm();
        verify(bot, times(0)).sendMessage(Mockito.anyInt(), Mockito.anyString());
    }
}
