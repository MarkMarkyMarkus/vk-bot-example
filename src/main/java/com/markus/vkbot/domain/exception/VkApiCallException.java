package com.markus.vkbot.domain.exception;

public final class VkApiCallException extends RuntimeException {
    public VkApiCallException(final String msg) {
        super(msg);
    }
}
