package com.markus.vkbot.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import static lombok.AccessLevel.PRIVATE;

@Value
@NoArgsConstructor(force = true, access = PRIVATE)
@AllArgsConstructor
public class VkApiCallResponse {
    String response;
    VkApiCallError error;

    @Value
    @NoArgsConstructor(force = true, access = PRIVATE)
    @AllArgsConstructor
    public static class VkApiCallError {
        @JsonProperty("error_code")
        int errorCode;
        @JsonProperty("error_msg")
        String errorMessage;

        @Override
        public String toString() {
            return "errorCode=" + errorCode + ", errorMessage=" + errorMessage;
        }
    }
}
