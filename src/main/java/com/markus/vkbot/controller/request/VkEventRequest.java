package com.markus.vkbot.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class VkEventRequest {
    String type;
    VkEventRequestObj object;
    @JsonProperty("group_id")
    int groupId;
}
