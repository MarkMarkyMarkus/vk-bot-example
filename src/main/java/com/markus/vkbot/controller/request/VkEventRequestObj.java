package com.markus.vkbot.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.Instant;

import static lombok.AccessLevel.PRIVATE;

@Value
@NoArgsConstructor(force = true, access = PRIVATE)
@AllArgsConstructor
public class VkEventRequestObj {
    PrivateMessage message;

    @Value
    public static class PrivateMessage {
        int id;
        Instant date;
        @JsonProperty("from_id")
        int fromId;
        @JsonProperty("peer_id")
        int peerId;
        String text;
    }
}
